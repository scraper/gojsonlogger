package gojsonlogger

import (
	"time"
)

type LogEntry struct {
	Date     string `json:date`
	LogLevel string `json:"logLevel"`
	AppName  string `json:"app"`
	LogId    int    `json:"logId"`
	Message  string `json:"msg"`
	Ip string `json:"ip"`
	Metadata interface{} `json:"info"`
}

type Logger interface {
	Error(message string)
	Errorf(message string,metadata interface{})
	Info(message string)
	Infof(message string,metadata interface{})
}

func MakeLogEntry(appName string, message string,ip string, logLevel string, logId int, metadata interface{}) LogEntry {
	return LogEntry{AppName: appName, LogLevel: logLevel, Ip: ip, LogId: logId, Message: message,
		Date: time.Now().Format(time.RFC3339),Metadata: metadata}
}
