package gojsonlogger

import (
	"encoding/json"
	"log"
	"os"
	"net"
	"errors"
)

type LogManager struct {
	logger *log.Logger
	ApplicationName string
	Ip string
}

func (l *LogManager) Init() {
	l.logger = log.New(os.Stdout, "", 0)
	l.Ip, _ = externalIP()
}

func (l *LogManager) Info(message string) {
	info := MakeLogEntry(l.ApplicationName, message,l.Ip,"INFO",0,nil)
	l.log(info)
}

func (l *LogManager) Infof(message string, metadata interface{}) {
	info := MakeLogEntry(l.ApplicationName, message,l.Ip,"INFO",0,metadata)
	l.log(info)
}

func (l *LogManager) Error(message string) {
	info := MakeLogEntry(l.ApplicationName, message,l.Ip,"ERROR",0, nil)
	l.log(info)
}

func (l *LogManager) Errorf(message string,metadata interface{}) {
	info := MakeLogEntry(l.ApplicationName, message,l.Ip,"ERROR",0, metadata)
	l.log(info)
}

func (l *LogManager) log(logEntry LogEntry) {
	result, _ := json.Marshal(logEntry)
	l.logger.Printf("%s\r\n", string(result))
}

func externalIP() (string, error) {
	ifaces, err := net.Interfaces()
	if err != nil {
		return "", err
	}
	for _, iface := range ifaces {
		if iface.Flags & net.FlagUp == 0 {
			continue // interface down
		}
		if iface.Flags & net.FlagLoopback != 0 {
			continue // loopback interface
		}
		addrs, err := iface.Addrs()
		if err != nil {
			return "", err
		}
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}
			if ip == nil || ip.IsLoopback() {
				continue
			}
			ip = ip.To4()
			if ip == nil {
				continue // not an ipv4 address
			}
			return ip.String(), nil
		}
	}
	return "", errors.New("are you connected to the network?")
}

