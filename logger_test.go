package gojsonlogger

import (
	"testing"
)

func Test_LogJsonArgument(t *testing.T) {

	logManager := LogManager{ApplicationName: "Test"}
	logManager.Init()

	// use anonymous struct to pass json arguments
	logManager.Infof("Hello",struct{ClientIp string}{"1.1.1.1"})

	// nested anonymous struct
	logManager.Errorf("Error",struct{ClientIp interface{}}{struct{Address string}{"1.2.3.4"}})
}